#! /usr/bin/python
# Pull data from NOAA.
import requests
data_url = 'https://www.nws.noaa.gov/mdl/gfslamp/lavlamp.shtml'
data = requests.get( data_url ).text

start = data.find( 'KCMI' )
end   = start + 1883

#print( data[ start:end ] )

# Find timestamp for data.
timestamp_start = data[ start:end ].find( 'GUIDANCE' ) + 10
timestamp_end   = timestamp_start + 14
timestamp_text  = data[ start:end ][ timestamp_start:timestamp_end ]
if timestamp_text[ 0 ] == ' ':  timestamp_text = '0' + timestamp_text[ 1: ]

#print( timestamp_text )

import datetime
timestamp = datetime.datetime.strptime( timestamp_text,'%m/%d/%Y  %H' )

# Write data to disk.
filename = str( timestamp.strftime( "%Y-%m-%d_%H00_UTC" ) ) + '.table'
with open( filename,'w' ) as outfile:
    outfile.write( data[ start:end ] )

